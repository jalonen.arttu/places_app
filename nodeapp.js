import http from 'http';
import path from 'path';
import fs from 'fs';

import React from 'react';
import express from 'express';
import ReactDOMServer from 'react-dom/server';

import Reactapp from './reactapp/app';

const app = express();
const port = 3000;


app.get('api/v1/places', (req, res) => {
    http.get('http://open-api.myhelsinki.fi/v1/places/', (response) => {
        var chuncks = '';
        response.on('data', function (data) {
            chuncks += data;
        });

        response.on('end', function () {
            try {
                const rawdata = JSON.parse(chuncks);
                var places = rawdata.data;
                var now = new Date();
                var today = now.getDay();
                //Fix week day number to fit myhelsinki openapi format
                (today === 0 ? today = 6 : --today )
                //go through places and give boolean value whether open or not
                for(var i = 0; i < places.length; i++) {
                    //TODO: Format time to comparable form and check availability
                    if(places[i].opening_hours && places[i].opening_hours.hours){
                        places[i].open = !!places[i].opening_hours.hours[today].opens;
                    } else {
                        places[i].open = false;
                    }
                }
                res.json(places);
            } catch (e) {
                console.error(e.message);
            }
        });
    });
});


app.get('/', (req, res) => {
    const reactapp = ReactDOMServer.renderToString(<Reactapp />);

    const indexFile = path.resolve('./reactapp/index.html');
    fs.readFile(indexFile, 'utf8', (err, data) => {
        if (err) {
            console.error('Something went wrong:', err);
            return res.status(500).send('Woops');
        }
        return res.send(
            data.replace('<tbody id="places"></>', `<tbody id="places">${reactapp}</tbody>`)
        );
    });
});

app.use(express.static('./reactapp'));

app.listen(port, () => {
    console.log(`Server listening on the port::${port}`);
});
