import React from 'react';
import Async from 'react-async';

let places = [];

const loadPlaces = () =>
    fetch("/api/v1/places")
        .then(res => (res.ok ? res : Promise.reject(res)))
        .then(res => res.json())


export default () => {
    return (
            <Async promiseFn={loadPlaces}>
                {({ data, err, isLoading }) => {
                    if (isLoading) return "Loading..."
                    if (err) return `Something went wrong: ${err.message}`

                    if (data)
                        return (
                            places.map((place) => (
                                <tr key={place.id}>
                                    <th scope="row">{place.name.en}</th>
                                    <th>{place.location.address.street_address}, {place.location.postal_code} {place.location.locality}</th>
                                    <th>{place.open}</th>
                                </tr>
                            ))
                        )
                }}
            </Async>
    );
}
