const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = {
    entry: './nodeapp.js',

    target: 'node',

    externals: [nodeExternals()],

    output: {
        path: path.resolve('server'),
        filename: 'server.js'
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                use: 'babel-loader'
            }
        ]
    }
};